{
  imports = [
    ./browsing
    ./discord
    ./libreoffice
    ./productivity
    ./spotify
  ];
}
